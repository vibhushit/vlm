/////Open a command terminal with in **VLM** folder

`cd chaincode`

`go mod tidy`

`cd Automobile-network/`

`./startAutomobileNetwork.sh`

`cd ../Auto-App`

`go mod tidy`

`go run .`

Browse http://localhost:8080/ || if you are using vm url -> IPofYourVM:8080 eg: http://172.83.8.205:8080/

Please explore all the features and capabilities by logging in.

couchdb url : http://172.83.8.205:7984/_utils/ || admin | adminpw

//To stop

`ctrl+c`

`cd ../Automobile-network/`

`./stopAutomobileNetwork.sh`

