package main

// Config represents the configuration for a role.
type Config struct {
	CertPath     string `json:"certPath"`
	KeyDirectory string `json:"keyPath"`
	TLSCertPath  string `json:"tlsCertPath"`
	PeerEndpoint string `json:"peerEndpoint"`
	GatewayPeer  string `json:"gatewayPeer"`
	MSPID        string `json:"mspID"`
}

// Create a Profile map
var profile = map[string]Config{

	"manufacturer": {
		CertPath:     "../Automobile-network/organizations/peerOrganizations/manufacturer.vlm.com/users/User1@manufacturer.vlm.com/msp/signcerts/cert.pem",
		KeyDirectory: "../Automobile-network/organizations/peerOrganizations/manufacturer.vlm.com/users/User1@manufacturer.vlm.com/msp/keystore/",
		TLSCertPath:  "../Automobile-network/organizations/peerOrganizations/manufacturer.vlm.com/peers/peer0.manufacturer.vlm.com/tls/ca.crt",
		PeerEndpoint: "0.0.0.0:7051",
		GatewayPeer:  "peer0.manufacturer.vlm.com",
		MSPID:        "ManufacturerMSP",
	},

	"dealer": {
		CertPath:     "../Automobile-network/organizations/peerOrganizations/dealer.vlm.com/users/User1@dealer.vlm.com/msp/signcerts/cert.pem",
		KeyDirectory: "../Automobile-network/organizations/peerOrganizations/dealer.vlm.com/users/User1@dealer.vlm.com/msp/keystore/",
		TLSCertPath:  "../Automobile-network/organizations/peerOrganizations/dealer.vlm.com/peers/peer0.dealer.vlm.com/tls/ca.crt",
		PeerEndpoint: "0.0.0.0:9051",
		GatewayPeer:  "peer0.dealer.vlm.com",
		MSPID:        "DealerMSP",
	},

	"mvd": {
		CertPath:     "../Automobile-network/organizations/peerOrganizations/mvd.vlm.com/users/User1@mvd.vlm.com/msp/signcerts/cert.pem",
		KeyDirectory: "../Automobile-network/organizations/peerOrganizations/mvd.vlm.com/users/User1@mvd.vlm.com/msp/keystore/",
		TLSCertPath:  "../Automobile-network/organizations/peerOrganizations/mvd.vlm.com/peers/peer0.mvd.vlm.com/tls/ca.crt",
		PeerEndpoint: "0.0.0.0:11051",
		GatewayPeer:  "peer0.mvd.vlm.com",
		MSPID:        "MvdMSP",
	},
	"ins": {
		CertPath:     "../Automobile-network/organizations/peerOrganizations/ins.vlm.com/users/User1@ins.vlm.com/msp/signcerts/cert.pem",
		KeyDirectory: "../Automobile-network/organizations/peerOrganizations/ins.vlm.com/users/User1@ins.vlm.com/msp/keystore/",
		TLSCertPath:  "../Automobile-network/organizations/peerOrganizations/ins.vlm.com/peers/peer0.ins.vlm.com/tls/ca.crt",
		PeerEndpoint: "0.0.0.0:12051",
		GatewayPeer:  "peer0.ins.vlm.com",
		MSPID:        "InsMSP",
	},
	"org1": {
		CertPath:     "../../fabric-samples/test-network/organizations/peerOrganizations/org1.example.com/users/User1@org1.example.com/msp/signcerts/cert.pem",
		KeyDirectory: "../../fabric-samples/test-network/organizations/peerOrganizations/org1.example.com/users/User1@org1.example.com/msp/keystore/",
		TLSCertPath:  "../../fabric-samples/test-network/organizations/peerOrganizations/org1.example.com/peers/peer0.org1.example.com/tls/ca.crt",
		PeerEndpoint: "0.0.0.0:7051",
		GatewayPeer:  "peer0.org1.example.com",
		MSPID:        "Org1MSP",
	},

	"org2": {
		CertPath:     "../../fabric-samples/test-network/organizations/peerOrganizations/org2.example.com/users/User1@org2.example.com/msp/signcerts/cert.pem",
		KeyDirectory: "../../fabric-samples/test-network/organizations/peerOrganizations/org2.example.com/users/User1@org2.example.com/msp/keystore/",
		TLSCertPath:  "../../fabric-samples/test-network/organizations/peerOrganizations/org2.example.com/peers/peer0.org2.example.com/tls/ca.crt",
		PeerEndpoint: "0.0.0.0:9051",
		GatewayPeer:  "peer0.org2.example.com",
		MSPID:        "Org2MSP",
	},

	"org3": {
		CertPath:     "../../fabric-samples/test-network/organizations/peerOrganizations/org3.example.com/users/User1@org3.example.com/msp/signcerts/cert.pem",
		KeyDirectory: "../../fabric-samples/test-network/organizations/peerOrganizations/org3.example.com/users/User1@org3.example.com/msp/keystore/",
		TLSCertPath:  "../../fabric-samples/test-network/organizations/peerOrganizations/org3.example.com/peers/peer0.org3.example.com/tls/ca.crt",
		PeerEndpoint: "0.0.0.0:11051",
		GatewayPeer:  "peer0.org3.example.com",
		MSPID:        "Org3MSP",
	},
}
